package FlaggedProjection;

import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.Polygon4d;
import org.junit.jupiter.api.Test;

import javax.vecmath.*;
import java.awt.*;
import java.util.Arrays;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FlaggedClippingManagerTest {

	private class FakeUnitClipper implements IFlaggedUnitClipper {

		@Override
		public Point2d[] clipToUnitShape(Point2d[] polygonVerts, int[] vertFlags) {
			return polygonVerts;
		}

		@Override
		public void setClippingManager(IClippingManager clippingManager) {}

		@Override
		public void setInversionManager(IInversionManager inversionManager) {}
	}


	@Test
	void clipPolygonsToCell() {

		Point3d origin = new Point3d(0,0,0);
		Vector3d basis1 = new Vector3d(2,0,0);
		Vector3d basis2 = new Vector3d(0,2,0);
		IPlane plane = new Plane(origin, basis1, basis2);

		Point4d p1 = new Point4d(11,12,13,787);
		Point4d p2 = new Point4d(21,22,23,978);
		Point4d p3 = new Point4d(31,32,33,476);
		Polygon4d[] polygons = { new Polygon4d(p1,p2,p3) };

		IFlaggedClippingManager manager = new FlaggedClippingManager();
		manager.setFlaggedUnitClipper(new FakeUnitClipper());
		Map.Entry<IPolygon3d[], Color[]> actual = manager.clipPolygonsToCell(Arrays.asList(polygons), null, plane);

		// expected 1: (11,12,13) -> (11/2,12/2) -> (11,12,0)
		// expected 2: (21,22,23) -> (21/2,22/2) -> (21,22,0)
		// expected 3: (31,32,33) -> (31/2,32/2) -> (31,32,0)
		assertEquals(1, actual.getKey().length);
		Point3d[] expectedVerts = {
			new Point3d(11,12,0),
			new Point3d(21,22,0),
			new Point3d(31,32,0)
		};
		assertArrayEquals(expectedVerts, actual.getKey()[0].getVertices());
	}

	@Test
	void clipPolygonToCell() {

		Point4d p1 = new Point4d(11,12,13, 1);
		Point4d p2 = new Point4d(21,22,23, -1);
		Point4d p3 = new Point4d(31,32,33, 1);
		Polygon4d flaggedPolygon = new Polygon4d(p1,p2,p3);

		Matrix4d mat1 = new Matrix4d(
			2,0,0,0,
			0,2,0,0,
			0,0,2,0,
			0,0,0,1
		);

		Matrix4d mat2 = new Matrix4d(
			1,0,0,1,
			0,1,0,2,
			0,0,1,3,
			0,0,0,1
		);

		IFlaggedClippingManager manager = new FlaggedClippingManager();
		manager.setFlaggedUnitClipper(new FakeUnitClipper());

		Matrix4d[] mats = {mat1, mat2};
		IPolygon3d actual = manager.clipPolygonToCell(flaggedPolygon, mats);

		// expected 1: (11,12,13) -> (22,24) -> (23,26,3)
		// expected 2: (21,22,23) -> (42,44) -> (43,46,3)
		// expected 3: (31,32,33) -> (62,64) -> (63,66,3)
		Point3d[] expectedVerts = {
			new Point3d(23,26,3),
			new Point3d(43,46,3),
			new Point3d(63,66,3)
		};
		assertArrayEquals(expectedVerts, actual.getVertices());
	}
}