package FlaggedProjection;

import Mesh.IMeshLine;
import Mesh.IMeshPolygon;
import Mesh.MeshLine;
import Mesh.MeshPolygon;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.jupiter.api.Assertions.*;

class CellTest {

	private final Point3d ORIGIN = new Point3d(11,12,13);
	private final Vector3d BASIS1 = new Vector3d(21,22,23);
	private final Vector3d BASIS2 = new Vector3d(31,32,33);
	private final IPlane PLANE = new Plane(ORIGIN, BASIS1, BASIS2);

	private final Point3d P1 = ORIGIN;
	private final Point3d P2 = new Point3d(32,34,36);
	private final Point3d P3 = new Point3d(63,66,69);
	private final Point3d P4 = new Point3d(42,44,46);

	@Test
	void planeMeshTypeConstructor() {
		IMeshLine[] edges = {
			new MeshLine(P1,P2),
			new MeshLine(P2,P4),
			new MeshLine(P4,P1),

		};
		IMeshPolygon[] faces = {
			new MeshPolygon(new Point3d[] {P1,P2,P4})
		};
		ICell cell = new Cell(PLANE);

		assertEquals(PLANE, cell.getPlane());
		assertArrayEquals(cell.getEdges(), edges);
		assertArrayEquals(cell.getFaces(), faces);
	}

	@Test
	void copyConstructor() {
		ICell cell1 = new Cell(PLANE);
		ICell cell2 = new Cell(cell1);

		assertEquals(cell1, cell2);
	}

	@Test
	void getPlane() {
		ICell cell = new Cell(PLANE);
		IPlane actual = cell.getPlane();
		assertEquals(PLANE, actual);
	}

	@Test
	void equals() {
		IPlane otherPlane = new Plane(ORIGIN, BASIS2, BASIS1);

		ICell currCell = new Cell(PLANE);

		for(boolean samePlane : new boolean[] {true, false}) {
			ICell otherCell = new Cell(samePlane ? PLANE : otherPlane);
			if(samePlane) assertEquals(currCell, otherCell);
			else assertNotEquals(currCell, otherCell);
		}
	}

}