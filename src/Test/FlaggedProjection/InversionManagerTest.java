package FlaggedProjection;

import Geometry.Lines.ILine2d;
import Geometry.Lines.Line2d;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point2d;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class InversionManagerTest {

	private final Point2d P1 = new Point2d(0.2, 0.2); // inside
	private final Point2d P2 = new Point2d(0.5, 0.5); // inside
	private final Point2d P3 = new Point2d(-0.5, 0.5); // outside
	private final Point2d P4 = new Point2d(-0.5, -0.5); // outside

	private final ILine2d LINE1 = new Line2d(P1, P2); // inside
	private final ILine2d LINE2 = new Line2d(P2, P3); // inside-outside
	private final ILine2d LINE3 = new Line2d(P3, P4); // outside
	private final ILine2d LINE4 = new Line2d(P4, P1); // outside-inside

	private final IInversionManager MANAGER = new InversionManager();

	@Test
	void invertLines() {

		List<ILine2d> lines = Arrays.asList(LINE1, LINE2, LINE3, LINE4);
		int[] flag1 = {1,1};
		List<int[]> flags1 = Arrays.asList(flag1,flag1,flag1,flag1);
		List<ILine2d> actual1 = MANAGER.invertLines(lines, flags1);
		assertEquals(4, actual1.size());
		for(int i=0; i<4; i++) {
			assertEquals(lines.get(i), actual1.get(i));
		}

		int[] flag2 = {1,-1};
		List<int[]> flags2 = Arrays.asList(flag2,flag2,flag2,flag2);
		List<ILine2d> actual2 = MANAGER.invertLines(lines, flags2);
		assertEquals(4, actual2.size());
		for(int i=0; i<4; i++) {
			assertEquals(lines.get(i).getP1(), actual2.get(i).getP1());
			assertNotEquals(lines.get(i).getP2(), actual2.get(i).getP2());
		}

		int[] flag3 = {-1,-1};
		List<int[]> flags3 = Arrays.asList(flag3,flag3,flag3,flag3);
		List<ILine2d> actual3 = MANAGER.invertLines(lines, flags3);
		assertEquals(0, actual3.size());

		int[] flag4 = {-1,1};
		List<int[]> flags4 = Arrays.asList(flag4,flag4,flag4,flag4);
		List<ILine2d> actual4 = MANAGER.invertLines(lines, flags4);
		assertEquals(4, actual4.size());
		for(int i=0; i<4; i++) {
			assertNotEquals(lines.get(i).getP1(), actual4.get(i).getP1());
			assertEquals(lines.get(i).getP2(), actual4.get(i).getP2());
		}

	}

	@Test
	void invertLine() {

		for(ILine2d line : new ILine2d[] {LINE1, LINE2, LINE3, LINE4}) {
			int[] flags = {1,1};
			ILine2d actual = MANAGER.invertLine(line, flags);
			assertEquals(line.getP1(), actual.getP1());
			assertEquals(line.getP2(), actual.getP2());
		}

		for(ILine2d line : new ILine2d[] {LINE1, LINE2, LINE3, LINE4}) {
			int[] flags = {1,-1};
			ILine2d actual = MANAGER.invertLine(line, flags);
			assertEquals(line.getP1(), actual.getP1());
			assertNotEquals(line.getP2(), actual.getP2());
		}

		for(ILine2d line : new ILine2d[] {LINE1, LINE2, LINE3, LINE4}) {
			int[] flags = {-1,-1};
			ILine2d actual = MANAGER.invertLine(line, flags);
			assertNull(actual);
		}

		for(ILine2d line : new ILine2d[] {LINE1, LINE2, LINE3, LINE4}) {
			int[] flags = {-1,1};
			ILine2d actual = MANAGER.invertLine(line, flags);
			assertNotEquals(line.getP1(), actual.getP1());
			assertEquals(line.getP2(), actual.getP2());
		}
	}
}