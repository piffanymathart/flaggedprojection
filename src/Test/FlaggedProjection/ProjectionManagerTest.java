package FlaggedProjection;

import org.junit.jupiter.api.Test;

import javax.vecmath.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ProjectionManagerTest {

	private final IProjectionManager MANAGER = new ProjectionManager();

	@Test
	void computeProjectionMatrix_fartherMeansSmaller() {

		double fovY = Math.PI/3.0;
		double aspect = 1.0;

		IFrustum frustum = new Frustum(fovY, aspect);
		Matrix4d mat = MANAGER.computeProjectionMatrix(frustum);

		Vector2d[] result = new Vector2d[4];
		for(int i=0; i<4; i++) {
			Point4d p = new Point4d(10, 10, -Math.pow(10,i+1), 1);
			mat.transform(p);
			Vector2d p2d = new Vector2d(p.x / p.w, p.y / p.w);
			result[i] = p2d;
		}

		assert(result[0].length() > result[1].length());
		assert(result[1].length() > result[2].length());
		assert(result[2].length() > result[3].length());
	}

	@Test
	void computeProjectionMatrix_largerFovYMeansMoreSquishedInY() {

		double aspect = 1.0;

		Vector2d[] result = new Vector2d[3];
		for(int i=0; i<3; i++) {
			double fovY = 0.25*(double)(i+1)*Math.PI;
			Matrix4d mat = MANAGER.computeProjectionMatrix(
				new Frustum(fovY, aspect)
			);
			Point4d p = new Point4d(10,10,-10,1);
			mat.transform(p);
			Vector2d p2d = new Vector2d(p.x/p.w, p.y/p.w);
			result[i] = p2d;
		}

		assert(result[0].y > result[1].y);
		assert(result[1].y > result[2].y);
	}

	@Test
	void computeProjectionMatrix_largerAspectMeansMoreSquishedInX() {

		// rectangle height is fixed (since we fix fovY)
		// larger aspect ratio means the rectangle is wider
		// since it sees more things in view, everything will be more squished
		// hence the x-values will be smaller

		double fovY = Math.PI/3.0;

		Vector2d[] result = new Vector2d[3];
		for(int i=0; i<3; i++) {
			double aspect = 0.5*(double)(i+1);
			Matrix4d mat = MANAGER.computeProjectionMatrix(
				new Frustum(fovY, aspect)
			);
			Point4d p = new Point4d(10,10,-10,1);
			mat.transform(p);
			Vector2d p2d = new Vector2d(p.x/p.w, p.y/p.w);
			result[i] = p2d;
		}

		assert(result[0].x > result[1].x);
		assert(result[1].x > result[2].x);
		assertEquals(result[0].y, result[1].y, 1e-5);
		assertEquals(result[1].y, result[2].y, 1e-5);
	}

	@Test
	void computeChangeOfBasisMatrix() {

		Point3d origin = new Point3d(11, 12, 13);
		Vector3d basis1 = new Vector3d(2, 0, 1);
		Vector3d basis2 = new Vector3d(1, 0, 4);
		Vector3d normal = new Vector3d();
		normal.cross(basis1, basis2);

		assert(normal.x == 0 && normal.y != 0 && normal.z == 0);

		Matrix4d mat = MANAGER.computeChangeOfBasisMatrix(origin, basis1, basis2);

		Point3d[] expectedPts = {
			new Point3d(40,50,60),
			new Point3d(0,0,-3),
			new Point3d(10,-40, 88)
		};

		for(Point3d expected : expectedPts) {

			Point4d coeffs = new Point4d(expected);
			mat.transform(coeffs);

			Point3d actual = new Point3d(
				coeffs.x * basis1.x + coeffs.y * basis2.x + coeffs.z * normal.x + coeffs.w * origin.x,
				coeffs.x * basis1.y + coeffs.y * basis2.y + coeffs.z * normal.y + coeffs.w * origin.y,
				coeffs.x * basis1.z + coeffs.y * basis2.z + coeffs.z * normal.z + coeffs.w * origin.z
			);

			assertEquals(expected.x, actual.x, 1e-5);
			assertEquals(expected.y, actual.y, 1e-5);
			assertEquals(expected.z, actual.z, 1e-5);
		}
	}

	@Test
	void computeViewportMatrix() {

		double[] oldViewport = {2, 4, 6, 8};
		double[] newViewport = {-3, 3, -5, -7};

		Matrix3d mat = MANAGER.computeViewportMatrix(oldViewport, newViewport);

		Point2d[] pts = {
			new Point2d(3,7),
			new Point2d(0, 10)
		};

		Point2d[] expectedPts = {
			new Point2d(0, -6),
			new Point2d(-9, -9)
		};

		for(int i=0; i<pts.length; i++) {

			Point2d actual = applyTransform(pts[i], mat);
			assertEquals(expectedPts[i], actual);
		}

		double[] badViewportX = {2, 2, 6, 8};
		double[] badViewportY = {2, 4, 6, 6};
		assertThrows(
			RuntimeException.class,
			() -> MANAGER.computeViewportMatrix(oldViewport, badViewportX)
		);
		assertThrows(
			RuntimeException.class,
			() -> MANAGER.computeViewportMatrix(badViewportY, newViewport)
		);
	}

	private Point2d applyTransform(Point2d p, Matrix3d matrix) {
		Point3d p3d = new Point3d(p.x, p.y, 1);
		matrix.transform(p3d);
		Point2d newPt = new Point2d(p3d.x, p3d.y);
		return newPt;
	}
}