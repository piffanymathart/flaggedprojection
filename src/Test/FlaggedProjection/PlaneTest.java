package FlaggedProjection;

import org.junit.jupiter.api.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PlaneTest {

	private final Point3d ORIGIN = new Point3d(11, 12, 13);
	private final Vector3d BASIS1 = new Vector3d(2, 0, 1);
	private final Vector3d BASIS2 = new Vector3d(1, 0, 4);

	@Test
	void OriginBasesConstructor() {
		IPlane plane = new Plane(ORIGIN, BASIS1, BASIS2);
		assertEquals(ORIGIN, plane.getOrigin());
		assertEquals(BASIS1, plane.getBasis1());
		assertEquals(BASIS2, plane.getBasis2());
	}

	@Test
	void pointConstructor() {
		Point3d p1 = new Point3d(ORIGIN);
		Point3d p2 = new Point3d(ORIGIN);
		p2.add(BASIS1);
		Point3d p3 = new Point3d(ORIGIN);
		p3.add(BASIS2);

		IPlane plane = new Plane(p1, p2, p3);
		assertEquals(ORIGIN, plane.getOrigin());
		assertEquals(BASIS1, plane.getBasis1());
		assertEquals(BASIS2, plane.getBasis2());

		assertThrows(RuntimeException.class, () -> new Plane(p1,null,p3));
		assertThrows(RuntimeException.class, () -> new Plane(p1,p2,null));
	}

	@Test
	void copyConstructor() {
		IPlane plane1 = new Plane(ORIGIN, BASIS1, BASIS2);
		IPlane plane2 = new Plane(plane1);
		assertEquals(ORIGIN, plane2.getOrigin());
		assertEquals(BASIS1, plane2.getBasis1());
		assertEquals(BASIS2, plane2.getBasis2());
	}

	@Test
	void getOrigin() {
		IPlane plane = new Plane(ORIGIN, BASIS1, BASIS2);
		assertEquals(ORIGIN, plane.getOrigin());
	}

	@Test
	void getBasis1() {
		IPlane plane = new Plane(ORIGIN, BASIS1, BASIS2);
		assertEquals(BASIS1, plane.getBasis1());
	}

	@Test
	void getBasis2() {
		IPlane plane = new Plane(ORIGIN, BASIS1, BASIS2);
		assertEquals(BASIS2, plane.getBasis2());
	}

	/**
	 * Verifies that the getNormal method returns the correct vector when called on a proper 2D plane.
	 */
	@Test
	void getNormal_Plane3d_returnsNormalVector() {
		IPlane plane = new Plane(ORIGIN, BASIS1, BASIS2);
		Vector3d actual = plane.getNormal();
		assert(actual.x == 0 && actual.y != 0 && actual.z == 0);
		assertEquals(1, actual.length());
	}

	/**
	 * Verifies that the getNormal method throws an exception when called on a degenerate 0D plane.
	 */
	@Test
	void getNormal_Plane1d_throwsException() {
		IPlane plane = new Plane(ORIGIN, BASIS1, BASIS1);
		assertThrows(RuntimeException.class, () -> plane.getNormal());
	}

	@Test
	void getNormal_Plane0d_throwsException() {
		IPlane plane = new Plane(ORIGIN, new Vector3d(), new Vector3d());
		assertThrows(RuntimeException.class, () -> plane.getNormal());
	}

	@Test
	void equals_same() {
		IPlane plane1 = new Plane(ORIGIN, BASIS1, BASIS1);
		IPlane plane2 = new Plane(ORIGIN, BASIS1, BASIS1);
		assert(plane1.equals(plane1));
		assert(plane1.equals(plane2));
	}

	@Test
	void equals_different() {
		Point3d otherOrigin = new Point3d(34,67,89);
		Vector3d otherBasis1 = new Vector3d(8,4,4);
		Vector3d otherBasis2 = new Vector3d(0,8,3);
		IPlane plane1 = new Plane(ORIGIN, BASIS1, BASIS1);
		IPlane plane2 = new Plane(otherOrigin, BASIS1, BASIS1);
		IPlane plane3 = new Plane(ORIGIN, otherBasis1, BASIS1);
		IPlane plane4 = new Plane(ORIGIN, BASIS1, otherBasis2);
		assert(!plane1.equals(null));
		assert(!plane1.equals(new Object()));
		assert(!plane1.equals(plane2));
		assert(!plane1.equals(plane3));
		assert(!plane1.equals(plane4));
	}

}