package FlaggedProjection;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class FrustumTest {

	private final double FOV_Y = 1.3;
	private final double ASPECT = 2.1;

	@Test
	void copyConstructor() {
		IFrustum frustum1 = new Frustum(FOV_Y, ASPECT);
		IFrustum frustum2 = new Frustum(frustum1);
		assertEquals(FOV_Y, frustum2.getFovY());
		assertEquals(ASPECT, frustum2.getAspect());
	}

	@Test
	void getFovY() {
		IFrustum frustum = new Frustum(FOV_Y, ASPECT);
		assertEquals(FOV_Y, frustum.getFovY());
	}

	@Test
	void getAspect() {
		IFrustum frustum = new Frustum(FOV_Y, ASPECT);
		assertEquals(ASPECT, frustum.getAspect());
	}

	@Test
	void getNear() {
		IFrustum frustum = new Frustum(FOV_Y, ASPECT);
		assertEquals(-1e-5, frustum.getNear());
	}

	@Test
	void setFovY() {
		IFrustum frustum = new Frustum(FOV_Y, ASPECT);

		frustum.setFovY(0.5);
		assertEquals(0.5, frustum.getFovY());

		frustum.setFovY(0);
		assertEquals(Frustum.MIN_FOV_Y, frustum.getFovY());

		frustum.setFovY(100);
		assertEquals(Frustum.MAX_FOV_Y, frustum.getFovY());
	}

	@Test
	void setAspect() {
		IFrustum frustum = new Frustum(FOV_Y, ASPECT);
		double newAspect = 0.4;
		frustum.setAspect(newAspect);
		assertEquals(newAspect, frustum.getAspect());
	}

	@Test
	void isValid() {

		double fovY = Math.PI/2.0;
		double aspect = 2.5;

		assertFalse(new Frustum(-0.1, aspect).isValid());
		assertFalse(new Frustum(3.3, aspect).isValid());
		assertFalse(new Frustum(fovY, -2).isValid());
	}
}