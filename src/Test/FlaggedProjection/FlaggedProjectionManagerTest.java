package FlaggedProjection;

import Geometry.Lines.ILine3d;
import Geometry.Lines.Line3d;
import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.IPolygon4d;
import Geometry.Polygons.Polygon3d;
import Mesh.IMesh;
import Mesh.IMeshPolygon;
import Mesh.Mesh;
import Mesh.MeshPolygon;
import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Point4d;
import javax.vecmath.Vector3d;
import java.awt.*;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class FlaggedProjectionManagerTest {

	private final IFlaggedProjectionManager MANAGER = new FlaggedProjectionManager();

	// xz-plane with width of 20 (x dir) and height of 40 (z dir) at y = -1, centred at (0,-1,0)
	private final Point3d PLANE_PT = new Point3d(-10,-1,20);
	private final Vector3d PLANE_V1 = new Vector3d(20,0,0);
	private final Vector3d PLANE_V2 = new Vector3d(0,0,-40);
	private final Vector3d PLANE_NORMAL = new Vector3d(0,1,0);

	private class IdentityFlaggedClippingManager implements IFlaggedClippingManager {

		@Override
		public Map.Entry<IPolygon3d[], Color[]> clipPolygonsToCell(List<IPolygon4d> flaggedPolygons, List<Color> polygonColours, IPlane plane) {
			int n = flaggedPolygons.size();
			IPolygon3d[] polygons = new Polygon3d[n];
			for(int i=0; i<n; i++) {
				Point4d[] vs4d = flaggedPolygons.get(i).getVertices();
				Point3d[] vs3d = new Point3d[vs4d.length];
				for(int j=0; j<vs4d.length; j++) {
					Point4d v4d = vs4d[j];
					vs3d[j] = new Point3d(v4d.x, v4d.y, v4d.z);
				}
				polygons[i] = new Polygon3d(vs3d);
			}
			return new AbstractMap.SimpleEntry<>(polygons, null);
		}

		@Override
		public IPolygon3d clipPolygonToCell(IPolygon4d flaggedPolygon, Matrix4d[] transforms) {
			return null;
		}

		@Override
		public void setFlaggedUnitClipper(IFlaggedUnitClipper flaggedUnitClipper) {}
	}

	@Test
	void projectAndClipToCell_degeneratePlane_returnsEmptyMesh() {

		// set up vertices
		Point3d r1 = new Point3d(3,-1,-3);
		Point3d r2 = new Point3d(4,-1,-2);
		Point3d r3 = new Point3d(5,-1,-1);
		Point3d p1 = new Point3d(r1); p1.scale(0.25);
		Point3d p2 = new Point3d(r2); p2.scale(0.5);
		Point3d p3 = new Point3d(r3); p3.scale(0.75);

		// set up edges and faces
		ILine3d edge = new Line3d(p1,p2);
		ILine3d[] edges = {edge};
		IPolygon3d face = new Polygon3d(p1,p2,p3);
		IPolygon3d[] faces = {face};

		// create mesh object to project
		IMesh obj = new Mesh(edges, faces, null);

		// set up cell to project onto
		IPlane plane = new Plane(PLANE_PT, PLANE_V1, PLANE_V1);
		ICell cell = new Cell(plane);

		// set up manager
		IFlaggedProjectionManager manager = new FlaggedProjectionManager();

		// test
		IMesh actual = manager.projectAndClipToCell(obj, cell);
		IMesh expected = new Mesh();
		assertEquals(expected, actual);
	}

	@Test
	void projectAndClipToCell_planeParallelToLineOfSight_returnsEmptyMesh() {

		// set up vertices
		Point3d r1 = new Point3d(3,-1,-3);
		Point3d r2 = new Point3d(4,-1,-2);
		Point3d r3 = new Point3d(5,-1,-1);
		Point3d p1 = new Point3d(r1); p1.scale(0.25);
		Point3d p2 = new Point3d(r2); p2.scale(0.5);
		Point3d p3 = new Point3d(r3); p3.scale(0.75);

		// set up edges and faces
		ILine3d edge = new Line3d(p1,p2);
		ILine3d[] edges = {edge};
		IPolygon3d face = new Polygon3d(p1,p2,p3);
		IPolygon3d[] faces = {face};

		// create mesh object to project
		IMesh obj = new Mesh(edges, faces, null);

		// set up cell to project onto
		IPlane plane = new Plane(new Point3d(0,0,0), new Point3d(0,0,-1), new Point3d(0,1,0));
		ICell cell = new Cell(plane);

		// set up manager
		IFlaggedProjectionManager manager = new FlaggedProjectionManager();

		// test
		IMesh actual = manager.projectAndClipToCell(obj, cell);
		IMesh expected = new Mesh();
		assertEquals(expected, actual);
	}

	@Test
	void projectAndClipToCell_insideFrustum_inFrontOfCell() {

		// set up vertices
		Point3d r1 = new Point3d(3,-1,-3);
		Point3d r2 = new Point3d(4,-1,-2);
		Point3d r3 = new Point3d(5,-1,-1);
		Point3d p1 = new Point3d(r1); p1.scale(0.25);
		Point3d p2 = new Point3d(r2); p2.scale(0.5);
		Point3d p3 = new Point3d(r3); p3.scale(0.75);

		// set up edges and faces
		ILine3d edge = new Line3d(p1,p2);
		ILine3d projEdge = new Line3d(r1,r2);
		ILine3d[] edges = {edge};
		IPolygon3d face = new Polygon3d(p1,p2,p3);
		IPolygon3d projFace = new Polygon3d(r1,r2,r3);
		IPolygon3d[] faces = {face};

		// create mesh object to project
		IMesh obj = new Mesh(edges, faces, null);

		// set up cell to project onto
		IPlane plane = new Plane(PLANE_PT, PLANE_V1, PLANE_V2);
		ICell cell = new Cell(plane);

		// set up manager
		IFlaggedProjectionManager manager = new FlaggedProjectionManager();
		IdentityFlaggedClippingManager clippingManager = new IdentityFlaggedClippingManager();
		manager.setFlaggedClippingManager(clippingManager);

		// test
		IMesh actual = manager.projectAndClipToCell(obj, cell);
		IMesh expected = new Mesh(new ILine3d[] {projEdge}, new IPolygon3d[] {projFace}, null);
		assertEquals(expected, actual);
	}

	@Test
	void projectAndClipToCell_outsideFrustum_behindCell() {

		// set up vertices
		Point3d r1 = new Point3d(3,-1,-3);
		Point3d r2 = new Point3d(4,-1,-2);
		Point3d r3 = new Point3d(5,-1,-1);
		Point3d p1 = new Point3d(r1); p1.scale(2);
		Point3d p2 = new Point3d(r2); p2.scale(3);
		Point3d p3 = new Point3d(r3); p3.scale(4);

		// set up edges and faces
		ILine3d edge = new Line3d(p1,p2);
		ILine3d projEdge = new Line3d(r1,r2);
		ILine3d[] edges = {edge};
		IPolygon3d face = new Polygon3d(p1,p2,p3);
		IPolygon3d projFace = new Polygon3d(r1,r2,r3);
		IPolygon3d[] faces = {face};

		// create mesh object to project
		IMesh obj = new Mesh(edges, faces, null);

		// set up cell to project onto
		IPlane plane = new Plane(PLANE_PT, PLANE_V1, PLANE_V2);
		ICell cell = new Cell(plane);

		// set up manager
		IFlaggedProjectionManager manager = new FlaggedProjectionManager();
		IdentityFlaggedClippingManager clippingManager = new IdentityFlaggedClippingManager();
		manager.setFlaggedClippingManager(clippingManager);

		// test
		IMesh actual = manager.projectAndClipToCell(obj, cell);
		IMesh expected = new Mesh(new ILine3d[] {projEdge}, new IPolygon3d[] {projFace}, null);
		assertEquals(expected, actual);
	}

	@Test
	void projectAndClipToCell_outsideFrustum() {

		// set up vertices
		Point3d r1 = new Point3d(3,-1,-3);
		Point3d r2 = new Point3d(4,-1,-2);
		Point3d r3 = new Point3d(5,-1,-1);
		Point3d p1 = new Point3d(r1); p1.scale(0.25); p1.y += 1000;
		Point3d p2 = new Point3d(r2); p2.scale(0.5); p2.y += 1000;
		Point3d p3 = new Point3d(r3); p3.scale(0.75); p3.y += 1000;

		// set up edges and faces
		ILine3d edge = new Line3d(p1,p2);
		ILine3d[] edges = {edge};
		IPolygon3d face = new Polygon3d(p1,p2,p3);
		IPolygon3d[] faces = {face};

		// create mesh object to project
		IMesh obj = new Mesh(edges, faces, null);

		// set up cell to project onto
		IPlane plane = new Plane(PLANE_PT, PLANE_V1, PLANE_V2);
		ICell cell = new Cell(plane);

		// set up manager
		IFlaggedProjectionManager manager = new FlaggedProjectionManager();
		IdentityFlaggedClippingManager clippingManager = new IdentityFlaggedClippingManager();
		manager.setFlaggedClippingManager(clippingManager);

		// test
		IMesh actual = manager.projectAndClipToCell(obj, cell);
		IMesh expected = new Mesh(new ILine3d[0], new IPolygon3d[0], null);
		assertEquals(expected, actual);
	}

	@Test
	void projectAndClipPolygons() {

		// set up polygons
		Point3d p1 = new Point3d(-12,2,-2);
		Point3d p2 = new Point3d(0,1,-4);
		Point3d r1 = new Point3d(6,-1,1);
		Point3d r2 = new Point3d(0,-1,4);
		IPolygon3d polygon1 = new Polygon3d(p1,p2);
		IPolygon3d polygon2 = new Polygon3d(p1);
		IPolygon3d[] polygons = {polygon1, polygon2};

		// set up manager
		IFlaggedProjectionManager manager = new FlaggedProjectionManager();
		IdentityFlaggedClippingManager clippingManager = new IdentityFlaggedClippingManager();
		manager.setFlaggedClippingManager(clippingManager);

		// set up cell
		IPlane plane = new Plane(PLANE_PT, PLANE_V1, PLANE_V2);
		ICell cell = mock(ICell.class);
		when(cell.getPlane()).thenReturn(plane);

		// test
		IMeshPolygon[] actual = manager.projectAndClipPolygons(polygons, null, cell);
		IMeshPolygon expectedPolygon = new MeshPolygon(new Point3d[] {r1,r2}, null, false, null);

		assertEquals(expectedPolygon, actual[0]);
	}

	@Test
	void projectPointToPlaneFast_inFront() {
		Point3d pt = new Point3d(-3,-0.5,0.5);
		Point4d expected = new Point4d(-6,-1,1,1);
		double planePtDotPlaneNormal = new Vector3d(PLANE_PT).dot(PLANE_NORMAL);
		Point4d actual = MANAGER.projectPointToPlaneFast(pt, PLANE_NORMAL, planePtDotPlaneNormal);
		assertEquals(expected, actual);
	}

	@Test
	void projectPointToPlaneFast_behind() {
		Point3d pt = new Point3d(0,1,-2);
		Point4d expected = new Point4d(0,-1,2,-1);
		double planePtDotPlaneNormal = new Vector3d(PLANE_PT).dot(PLANE_NORMAL);
		Point4d actual = MANAGER.projectPointToPlaneFast(pt, PLANE_NORMAL, planePtDotPlaneNormal);
		assertEquals(expected, actual);
	}
}