package FlaggedProjection;

import org.junit.jupiter.api.Test;

import javax.vecmath.Point2d;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClippingManagerTest {

	private final IClippingManager CLIPPER = new ClippingManager();

	private static final Point2d[] UNIT_TRIANGLE_VERTS = {
		new Point2d(0,0), new Point2d(1,0), new Point2d(0,1)
	};

	private final Point2d[] polygon = {
		new Point2d(0.5, 1.3),
		new Point2d(0.2, 0.4),
		new Point2d(1.2, 0.4),
		new Point2d(1.1, -0.2),
		new Point2d(0.4, -0.3),
		new Point2d(-0.2, 0.6)
	};

	@Test
	void clipPointsToPolygon_unitTriangle() {

		Point2d[] expected = {
			new Point2d(0.3, 0.7),
			new Point2d(0.2, 0.4),
			new Point2d(0.6, 0.4),
			new Point2d(1.0, 0.0),
			new Point2d(0.2, 0.0),
			new Point2d(0.0, 0.3),
			new Point2d(0.0, 0.8),
			new Point2d(0.1, 0.9)
		};

		// round the values to 5 decimals
		Point2d[] actual = CLIPPER.clipPointsToPolygon(polygon, UNIT_TRIANGLE_VERTS );

		assertSamePolygon(expected, actual, 1e-5);
	}

	@Test
	void clipPointsToPolygon_farawayPoints() {

		double x = 0.1;
		double y = 0.2;

		Point2d[] polygon1 = {
			new Point2d(x, y),
			new Point2d(x + 1,y + 2)
		};

		Point2d[] polygon2 = {
			new Point2d(x, y),
			new Point2d(x + 1e9,y + 2e9)
		};

		// round the values to 5 decimals
		Point2d[] expected = CLIPPER.clipPointsToPolygon(polygon1, UNIT_TRIANGLE_VERTS );
		Point2d[] actual = CLIPPER.clipPointsToPolygon(polygon2, UNIT_TRIANGLE_VERTS );

		assertSamePolygon(expected, actual, 1e-5);
	}


	private void assertSamePolygon(Point2d[] expected, Point2d[] actual, double tol) {
		actual = roundValues(actual, tol);
		expected = roundValues(expected, tol);

		int n = expected.length;
		assertEquals(n, actual.length);
		actual = roundValues(actual, tol);
		// find offset
		int offset = -1;
		for(int i=0; i<n; i++) {
			if(actual[i].equals(expected[0])) {
				offset = i;
				break;
			}
		}
		assert(offset!=-1);

		// check that the two lists are equal mod offset
		for(int i=0; i<n; i++) {
			int j = (i+offset)%n;
			assertEquals(expected[i], actual[j]);
		}
	}

	private Point2d[] roundValues(Point2d[] points, double tol) {

		int n = points.length;
		double factor = Math.round(1.0/tol);
		Point2d[] newPts = new Point2d[n];
		for(int i=0; i<n; i++) {
			double x = points[i].x;
			double y = points[i].y;
			double newX = Math.round(x*factor)/factor;
			double newY = Math.round(y*factor)/factor;
			newPts[i] = new Point2d(newX, newY);
		}
		return newPts;
	}
}