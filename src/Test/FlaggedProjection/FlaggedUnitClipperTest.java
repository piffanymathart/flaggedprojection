package FlaggedProjection;

import Geometry.Lines.ILine2d;
import org.junit.jupiter.api.Test;

import javax.vecmath.Point2d;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FlaggedUnitClipperTest {

	private class IdentityInversionManager implements IInversionManager {

		public int inversionCount = 0;

		@Override
		public List<ILine2d> invertLines(List<ILine2d> lines, List<int[]> flags) {
			inversionCount++;
			return lines;
		}

		@Override
		public ILine2d invertLine(ILine2d line, int[] flags) { return null; }
	}

	private class IdentityClippingManager implements IClippingManager {

		public int clipCount = 0;

		@Override
		public Point2d[] clipPointsToPolygon(Point2d[] points, Point2d[] clipper) {
			clipCount++;
			return points;
		}
	}

	private class Return3ClippingManager implements  IClippingManager {

		public int clipCount = 0;
		public final Point2d[] clippedPolygon = new Point2d[] {
			new Point2d(0,0), new Point2d(2,4), new Point2d(3,6),new Point2d(1,2)
		};
		public final Point2d[] clippedLine = new Point2d[] {new Point2d(0,0), new Point2d(3,6)};

		@Override
		public Point2d[] clipPointsToPolygon(Point2d[] points, Point2d[] clipper) {
			clipCount++;
			return clippedPolygon;
		}
	}

	@Test
	void clipToUnitShape_twoLines() {

		Point2d p1 = new Point2d(11,12);
		Point2d p2 = new Point2d(21,22);
		Point2d p3 = new Point2d(31,32);

		Point2d[] polygonVerts = {p1, p2, p3};
		int[] vertFlags = {345,23,456}; // doesn't matter, will mock out inversionManager

		IFlaggedUnitClipper flaggedUnitClipper = new FlaggedUnitClipper();
		IdentityClippingManager clippingManager = new IdentityClippingManager();
		IdentityInversionManager inversionManager = new IdentityInversionManager();
		flaggedUnitClipper.setClippingManager(clippingManager);
		flaggedUnitClipper.setInversionManager(inversionManager);

		Point2d[] expected = {p1, p2, p2, p3, p3, p1};
		Point2d[] actual = flaggedUnitClipper.clipToUnitShape(polygonVerts, vertFlags);
		assertArrayEquals(expected, actual);
		assertEquals(1, clippingManager.clipCount);
		assertEquals(1, inversionManager.inversionCount);
	}

	@Test
	void clipToUnitShape_oneLine() {
		Point2d p1 = new Point2d(11,12);
		Point2d p2 = new Point2d(21,22);

		Point2d[] polygonVerts = {p1, p2};
		int[] vertFlags = {345,23}; // doesn't matter, will mock out inversionManager

		IFlaggedUnitClipper flaggedUnitClipper = new FlaggedUnitClipper();
		Return3ClippingManager clippingManager = new Return3ClippingManager();
		IdentityInversionManager inversionManager = new IdentityInversionManager();
		flaggedUnitClipper.setClippingManager(clippingManager);
		flaggedUnitClipper.setInversionManager(inversionManager);

		Point2d[] expected = clippingManager.clippedLine;
		Point2d[] actual = flaggedUnitClipper.clipToUnitShape(polygonVerts, vertFlags);
		assertArrayEquals(expected, actual);
		assertEquals(1, clippingManager.clipCount);
		assertEquals(1, inversionManager.inversionCount);
	}
}