package FlaggedProjection;

import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.IPolygon4d;
import Geometry.Polygons.Polygon3d;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Point4d;
import java.awt.*;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * A helper class that manages clipping of lines and polygons with flagged coordinates
 * arising from perspective projection.
 */
class FlaggedClippingManager implements IFlaggedClippingManager {

	/**
	 * Manages the clipping of objects to unit triangle/square in flagged space.
	 */
	private IFlaggedUnitClipper m_flaggedUnitClipper = new FlaggedUnitClipper();

	/**
	 * Manages projections.
	 */
	private IProjectionManager m_projectionManager = new ProjectionManager();

	/**
	 * Clip a set of polygons to a cell.
	 * @param flaggedPolygons The flagged polygons to clip.
	 * @param plane The plane that defines the  cell.
	 * @return A set of clipped polygons.
	 */
	public Map.Entry<IPolygon3d[], Color[]> clipPolygonsToCell(List<IPolygon4d> flaggedPolygons, List<Color> polygonColours, IPlane plane) {

		// compute transformation matrices for converting between world and cell coordinates
		Matrix4d[] transforms = computeChangeOfBasisMatrices(plane);

		// loop over the polygons to clip
		List<IPolygon3d> clippedPolygons = new ArrayList<>();
		List<Color> clippedPolygonColours = new ArrayList<>();
		for(int i=0; i<flaggedPolygons.size(); i++) {

			// clip each polygon and add to collection if not empty
			IPolygon3d clippedPolygon = clipPolygonToCell( flaggedPolygons.get(i), transforms);
			if(clippedPolygon.size()>0) {
				clippedPolygons.add(clippedPolygon);
				if(polygonColours!=null) clippedPolygonColours.add( polygonColours.get(i));
			}
		}

		Map.Entry<IPolygon3d[], Color[]> result = new AbstractMap.SimpleEntry<>(
			clippedPolygons.toArray(new Polygon3d[clippedPolygons.size()]),
			(polygonColours==null) ? null : clippedPolygonColours.toArray(new Color[clippedPolygonColours.size()])
		);

		return result;
	}

	/**
	 * Clip a polygon to a cell.
	 * @param flaggedPolygon The flagged polygon to clip.
	 * @param transforms The matrix transform (and its inverse) that transform to cell coordinates (and back).
	 * @return A clipped polygon.
	 */
	public IPolygon3d clipPolygonToCell(IPolygon4d flaggedPolygon, Matrix4d[] transforms) {

		IPolygon3d polygon = getPolygon3d(flaggedPolygon);
		int[] flags = getPerspectiveFlags(flaggedPolygon);

		// polygon to vertices
		Point3d[] verts = polygon.getVertices();

		// transform to cell coordinates
		Point2d[] cellVerts = transformToCellCoordinates(verts, transforms[0]);

		// perform clipping
		Point2d[] clippedPts = m_flaggedUnitClipper.clipToUnitShape(cellVerts, flags);

		// convert back to world coordinates
		Point3d[] newPts = transformToWorldCoordinates(clippedPts, transforms[1]);

		// vertices to polygon
		return new Polygon3d(newPts);
	}

	/**
	 * Sets the flagged unit clipper.
	 * @param flaggedUnitClipper The flagged unit clipper to set as.
	 */
	public void setFlaggedUnitClipper(IFlaggedUnitClipper flaggedUnitClipper) {
		m_flaggedUnitClipper = flaggedUnitClipper;
	}

	/**
	 * Transforms a set of 2D points from cell to world coordinates.
	 * @param pts The 2D points to transform.
	 * @param transform The 4x4 affine transformation matrix
	 * @return The transformed 3D points.
	 */
	private Point3d[] transformToWorldCoordinates(Point2d[] pts, Matrix4d transform) {

		// iterate over array and transform each point
		int n = pts.length;
		Point3d[] newPts = new Point3d[n];
		for(int i=0; i<n; i++) {
			newPts[i] = transformToWorldCoordinates(pts[i], transform);
		}
		return newPts;
	}

	/**
	 * Transforms a 2D point from cell to world coordinates.
	 * @param p The 2D point to transform.
	 * @param transform The 4x4 affine transformation matrix
	 * @return The transformed 3D point.
	 */
	private Point3d transformToWorldCoordinates(Point2d p, Matrix4d transform) {
		Point3d p3d = new Point3d(p.x, p.y, 0);
		transform.transform(p3d);
		return p3d;
	}

	/**
	 * Transforms a set of 3D points from world to cell coordinates.
	 * @param pts The 3D points to transform.
	 * @param transform The 4x4 affine transformation matrix
	 * @return The transformed 2D points.
	 */
	private Point2d[] transformToCellCoordinates(Point3d[] pts, Matrix4d transform) {

		int n = pts.length;
		Point2d[] newVerts = new Point2d[n];
		for(int i=0; i<n; i++) {
			newVerts[i] = transformToCellCoordinates(pts[i], transform);
		}
		return newVerts;
	}

	/**
	 * Transforms a 3D points from world to cell coordinates.
	 * @param p The 3D point to transform.
	 * @param transform The 4x4 affine transformation matrix
	 * @return The transformed 2D point.
	 */
	private Point2d transformToCellCoordinates(Point3d p, Matrix4d transform) {
		transform.transform(p);
		return new Point2d(p.x, p.y);
	}

	/**
	 * Computes the change-of-basis matrix corresponding to the plane's origin and basis vectors.
	 * @param plane The plane defining the origin and basis vectors.
	 * @return The 4x4 affine transformation matrix defining the change of basis.
	 */
	private Matrix4d[] computeChangeOfBasisMatrices(IPlane plane) {

		Matrix4d matrix = m_projectionManager.computeChangeOfBasisMatrix(
			plane.getOrigin(), plane.getBasis1(), plane.getBasis2()
		);

		Matrix4d invMatrix = new Matrix4d(matrix);
		invMatrix.invert();

		return new Matrix4d[] {matrix, invMatrix};
	}

	/**
	 * Gets the list of perspective flags from the polygons.
	 * @param polygon The polygons containing perspective flags.
	 * @return The perspective flags.
	 */
	private int[] getPerspectiveFlags(IPolygon4d polygon) {
		int[] flags = new int[polygon.size()];
		for(int i=0; i<polygon.size(); i++) flags[i] = polygon.get(i).w > 0 ? 1 : -1;
		return flags;
	}

	/**
	 * Converts a 4D polygon into a 3D polygon by dropping the w-coordinates.
	 * @param poly4 The 4D polygon.
	 * @return The converted 3D polygon.
	 */
	private IPolygon3d getPolygon3d(IPolygon4d poly4) {
		int n = poly4.size();
		// loop over vertices and drop the w-coordinate
		Point3d[] verts = new Point3d[n];
		for(int i=0; i<n; i++) {
			Point4d v = poly4.get(i);
			verts[i] = new Point3d(v.x, v.y, v.z);
		}
		return new Polygon3d(verts);
	}
}
