package FlaggedProjection;

import javax.vecmath.Point2d;

/**
 * A helper class for clipping polygons to a plane.
 */
interface IFlaggedUnitClipper {

	/**
	 * Clips a set of polygon vertices to the unit triangle.
	 * @param polygonVerts The polygon vertices.
	 * @param vertFlags The flags on the polygon vertices.
	 * @return The set of clipped vertices.
	 */
	Point2d[] clipToUnitShape(Point2d[] polygonVerts, int[] vertFlags);

	/**
	 * Sets the clipping manager.
	 * @param clippingManager The clipping manager.
	 */
	void setClippingManager(IClippingManager clippingManager);

	/**
	 * Sets the inversion manager.
	 * @param inversionManager The inversion manager.
	 */
	void setInversionManager(IInversionManager inversionManager);
}
