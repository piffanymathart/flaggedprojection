package FlaggedProjection;

import Mesh.IMesh;

/**
 * A cell that makes up a larger projection screen.
 */
public interface ICell extends IMesh {

	/**
	 * Gets the plane.
	 * @return The plane.
	 */
	IPlane getPlane();

	/**
	 * Checks if the object is equal to the cell.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the cell.
	 */
	boolean equals(Object obj);
}
