package FlaggedProjection;

import Geometry.Lines.ILine2d;
import Geometry.Lines.Line2d;

import javax.vecmath.Point2d;
import java.util.ArrayList;
import java.util.List;

/**
 * A helper class for clipping polygons to a plane.
 */
class FlaggedUnitClipper implements IFlaggedUnitClipper {

	/**
	 * Vertices of a unit triangle used for clipping.
	 */
	private static final Point2d[] UNIT_TRIANGLE_VERTS = {
		new Point2d(0,0), new Point2d(1,0), new Point2d(0,1)
	};

	/**
	 * The manager that helps with clipping.
	 */
	private IClippingManager m_clippingManager = new ClippingManager();

	/**
	 * The manager that helps with inverting.
	 */
	private IInversionManager m_inversionManager = new InversionManager();


	/**
	 * Clips a set of polygon vertices to the unit triangle.
	 * @param polygonVerts The polygon vertices.
	 * @param vertProjFlags The projection flags on the polygon vertices.
	 * @return The set of clipped vertices.
	 */
	public Point2d[] clipToUnitShape(Point2d[] polygonVerts, int[] vertProjFlags) {

		// convert vertices to edges
		List<ILine2d> edges = pointsToLines(polygonVerts);
		List<int[]> edgeFlags = polygonProjFlagsToLinesProjFlags(vertProjFlags);
		// invert edges
		List<ILine2d> invertedPolygonLines = m_inversionManager.invertLines(edges, edgeFlags);
		// convert back to vertices for clipping
		Point2d[] invertedPolygonPts = linesToPoints(invertedPolygonLines);
		// clip to unit triangle or square
		Point2d[] clippedPolygonPts = m_clippingManager.clipPointsToPolygon(
			invertedPolygonPts,	UNIT_TRIANGLE_VERTS
		);

		// if there's only one line, then trim it to a proper line
		if(edges.size()==1 && clippedPolygonPts.length>2) {
			return pointsToLinePoints(clippedPolygonPts);
		}

		return clippedPolygonPts;
	}

	/**
	 * Sets the clipping manager.
	 * @param clippingManager The clipping manager.
	 */
	public void setClippingManager(IClippingManager clippingManager) {
		m_clippingManager = clippingManager;
	}

	/**
	 * Sets the inversion manager.
	 * @param inversionManager The inversion manager.
	 */
	public void setInversionManager(IInversionManager inversionManager) {
		m_inversionManager = inversionManager;
	}


	/**
	 * Converts a set of points to line segments.
	 * @param points The points.
	 * @return The lines.
	 */
	private List<ILine2d> pointsToLines(Point2d[] points) {
		int n = points.length;
		List<ILine2d> lines = new ArrayList<>();

		if(n==2) {
			lines.add(new Line2d(points[0], points[1]));
		}
		else if(n>2) {
			for(int i = 0; i < n; i++) {
				int j = (i == n-1) ? 0 : i+1;
				lines.add( new Line2d(points[i], points[j]));
			}
		}
		return lines;
	}

	/**
	 * Converts a set of points to vertices of line segments.
	 * @param points The points.
	 * @return An array of vertices of line segments.
	 */
	private Point2d[] pointsToLinePoints(Point2d[] points) {
		if(points.length==0) return null;
		// let p1 be the first point in the polygon
		Point2d p1 = points[0];
		double maxDist = 0;
		// let p2 be the point in the polygon farthest from p1
		Point2d p2 = new Point2d(p1);
		for(Point2d v : points) {
			double dist = v.distance(p1);
			if(dist > maxDist) {
				maxDist = dist;
				p2 = new Point2d(v);
			}
		}
		return new Point2d[] {p1,p2};
	}

	/**
	 * Converts projection flags on polygons to projection flags on lines.
	 * @param polygonFlags Projection flags on polygons.
	 * @return Projection flags on lines.
	 */
	private List<int[]> polygonProjFlagsToLinesProjFlags(int[] polygonFlags) {
		int n = polygonFlags.length;
		List<int[]> linesFlags = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			int j = (i == n - 1) ? 0 : i + 1;
			linesFlags.add(new int[] {polygonFlags[i], polygonFlags[j]});
		}
		return linesFlags;
	}

	/**
	 * Converts a set of lines to points.
	 * @param lines The lines.
	 * @return The points.
	 */
	private Point2d[] linesToPoints(List<ILine2d> lines) {
		int n = lines.size();
		Point2d[] points = new Point2d[2*n];
		for(int i=0; i<n; i++) {
			points[2*i] = lines.get(i).getP1();
			points[2*i+1] = lines.get(i).getP2();
		}
		return points;
	}
}