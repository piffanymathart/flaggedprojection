package FlaggedProjection;

import Geometry.Lines.ILine2d;
import Geometry.Lines.Line2d;

import javax.vecmath.Point2d;
import java.util.ArrayList;
import java.util.List;

/**
 * A helper class to manage the inversion of lines and polygons needed to
 * draw elements that are partially outside the projection plane's view.
 */
class InversionManager implements IInversionManager {

	/**
	 * Inverts a set of lines with their suggested projection flags.
	 * @param lines The lines.
	 * @param flags The corresponding suggested projection flags.
	 * @return The inverted lines.
	 */
	public List<ILine2d> invertLines(List<ILine2d> lines, List<int[]> flags) {

		List<ILine2d> invertedLines = new ArrayList<>();
		for(int i=0; i<lines.size(); i++) {
			ILine2d invertedLine = invertLine(lines.get(i), flags.get(i));
			if(invertedLine!=null) invertedLines.add(invertedLine);
		}
		return invertedLines;
	}

	/**
	 * Inverts a line with the projection flags of its endpoints.
	 * @param line The line.
	 * @param projFlags The projection flags of its endpoints.
	 * @return The inverted line.
	 */
	public ILine2d invertLine(ILine2d line, int[] projFlags) {
		Point2d p1 = line.getP1();
		Point2d p2 = line.getP2();
		double extendBy = 999999;

		int projFlag1 = projFlags[0];
		int projFlag2 = projFlags[1];

		if(projFlag1 < 0) {
			if(projFlag2 < 0) return null;
			else p1 = reflect(p1,p2,extendBy);
		}
		else if(projFlag2 < 0) p2 = reflect(p2,p1,extendBy);

		ILine2d invertedLine = new Line2d(p1, p2);
		return invertedLine;
	}

	/**
	 * Reflects a point across the anchor by some length.
	 * @param p The point to reflect.
	 * @param centre The centre of reflection.
	 * @param length The length between the centre and the reflected point.
	 * @return The reflected point.
	 */
	private static Point2d reflect(Point2d p, Point2d centre, double length) {

		double k = length/p.distance(centre);
		Point2d newP = new Point2d(
			centre.x + k*(centre.x - p.x),
			centre.y + k*(centre.y - p.y)
		);
		return newP;
	}
}
