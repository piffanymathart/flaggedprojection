package FlaggedProjection;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;


/**
 * A helper class for managing geometric transformations related to perspective projections.
 */
public interface IProjectionManager {

	/**
	 * Computes the affine transformation matrix for a perspective projection.
	 * @param frustum The frustum that defines the perspective projection.
	 * @return The 4x4 affine transformation matrix that performs the perspective projection.
	 */
	Matrix4d computeProjectionMatrix(IFrustum frustum);

	/**
	 * Computes the change-of-basis matrix for the given origin and vectors.
	 * @param origin The origin for the new basis.
	 * @param v1 The first basis vector.
	 * @param v2 The first basis vector.
	 * @return The 4x4 affine transformation matrix that performs the change of basis.
	 */
	Matrix4d computeChangeOfBasisMatrix(Point3d origin, Vector3d v1, Vector3d v2);

	/**
	 * Computes the transformation matrix that stretches objects in the old viewport to fit the new viewport.
	 * @param oldViewport The original 2D viewport given by [leftX, rightX, topY, bottomY].
	 * @param newViewport The new 2D viewport given by given by [leftX, rightX, topY, bottomY].
	 * @return The 3x3 affine transformation matrix that defines the rescaling.
	 */
	Matrix3d computeViewportMatrix(double[] oldViewport, double[] newViewport);

}
