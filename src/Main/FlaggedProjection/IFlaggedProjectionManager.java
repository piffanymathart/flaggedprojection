package FlaggedProjection;

import Geometry.Polygons.IPolygon3d;
import Mesh.IMesh;
import Mesh.IMeshPolygon;

import javax.vecmath.Point3d;
import javax.vecmath.Point4d;
import javax.vecmath.Vector3d;
import java.awt.*;

/**
 * A helper class for managing projections of flagged points, lines, and faces. A 3D flagged
 * point contains an extra 4th coordinate that we call the perspective flag. This flag indicates
 * whether a point is in front of (flag = 1) or behind the eye (flag = -1). It is a result of performing
 * a perspective projection.
 *
 * Typically, one would not render such a point because it does not lie on the projection plane. However,
 * if a line connects two points whose flag values are 1 and -1, then this line will partially lie on the
 * projection plane. Therefore we need to retain the point with flag = -1 in order to perform the line
 * clipping. Since this point does not have a physical position in 3-space, the 4th coordinate is required
 * to properly represent its position and state.
 */
public interface IFlaggedProjectionManager {

	/**
	 * Projects and clips a 3D model from the origin to a plane.
	 * @param obj The 3D model to project.
	 * @param cell The cell on which to project.
	 * @return The projected and clipped model on the cell.
	 */
	IMesh projectAndClipToCell(IMesh obj, ICell cell);

	/**
	 * Projects and clips a set of coloured polygons onto a cell.
	 * @param polygons The polygons.
	 * @param polygonColours The polygon colours.
	 * @param cell The cell.
	 * @return A pair of polygons with colours.
	 */
	IMeshPolygon[] projectAndClipPolygons(IPolygon3d[] polygons, Color[] polygonColours, ICell cell);

	/**
	 * Computes the projection of a 3D point from the origin to a plane using a faster merhod
	 * involving precomputed values.
	 * @param pt The 3D point to project.
	 * @param planeNormal The 3D normal vector of the plane.
	 * @param planePtDotNormal The dot product of the plane point and the outward plane normal.
	 * @return A flagged 3D point representing the projected point that lies on the target plane.
	 */
	Point4d projectPointToPlaneFast(Point3d pt, Vector3d planeNormal, double planePtDotNormal);

	/**
	 * Sets the flagged clipping manager.
	 * @param flaggedClippingManager The flagged clipping manager to set to.
	 */
	void setFlaggedClippingManager(IFlaggedClippingManager flaggedClippingManager);

}
