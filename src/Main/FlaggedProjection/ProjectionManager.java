package FlaggedProjection;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static java.lang.Math.tan;

/**
 * A helper class for managing geometric transformations related to perspective projections.
 */
public class ProjectionManager implements IProjectionManager {

	/**
	 * Computes the affine transformation matrix for a perspective projection.
	 * @param frustum The frustum that defines the perspective projection.
	 * @return The 4x4 affine transformation matrix that performs the perspective projection.
	 */
	public Matrix4d computeProjectionMatrix(IFrustum frustum) {

		if(!frustum.isValid()) throw new RuntimeException();

		// compute standard perspective projection matrix as viewed from the origin
		double invTanY = 1.0 / tan(frustum.getFovY() / 2.0);
		double invTanX = invTanY/frustum.getAspect();
		double nearZ = frustum.getNear();
		double farZ = 1e9;
		Matrix4d perspectiveMatrix = new Matrix4d(
			invTanX, 0, 0, 0,
			0, invTanY, 0, 0,
			0, 0, -(farZ+nearZ)/(farZ-nearZ), -(2.0*farZ*nearZ)/(farZ-nearZ),
			0, 0, -1, 0
		);
		return perspectiveMatrix;
	}

	/**
	 * Computes the change-of-basis matrix for the given origin and vectors.
	 * @param origin The origin for the new basis.
	 * @param v1 The first basis vector.
	 * @param v2 The first basis vector.
	 * @return The 4x4 affine transformation matrix that performs the change of basis.
	 */
	public Matrix4d computeChangeOfBasisMatrix(Point3d origin, Vector3d v1, Vector3d v2) {
		// compute the normal vector
		Vector3d normal = new Vector3d();
		normal.cross(v1, v2);
		// compute the standard change of basis, ignoring the new origin position
		Matrix3d mat = new Matrix3d(
			v1.x, v2.x, normal.x,
			v1.y, v2.y, normal.y,
			v1.z, v2.z, normal.z
		);
		mat.invert();
		Matrix4d augmentedMatrix = new Matrix4d(
			mat.m00, mat.m01, mat.m02, 0,
			mat.m10, mat.m11, mat.m12, 0,
			mat.m20, mat.m21, mat.m22, 0,
			0, 0, 0, 1
		);
		// account for the origin position
		Matrix4d translateMatrix = new Matrix4d(
			1, 0, 0, -origin.x,
			0, 1, 0, -origin.y,
			0, 0, 1, -origin.z,
			0, 0, 0, 1
		);
		Matrix4d matProd = new Matrix4d();
		matProd.mul(augmentedMatrix, translateMatrix);
		return matProd;
	}

	/**
	 * Computes the transformation matrix that stretches objects in the old viewport to fit the new viewport.
	 * @param oldViewport The original 2D viewport given by [leftX, rightX, topY, bottomY].
	 * @param newViewport The new 2D viewport given by given by [leftX, rightX, topY, bottomY].
	 * @return The 3x3 affine transformation matrix that defines the rescaling.
	 */
	public Matrix3d computeViewportMatrix(double[] oldViewport, double[] newViewport) {

		// validate parameters

		double xDiff1 = oldViewport[1] - oldViewport[0];
		if(xDiff1==0) throw new RuntimeException("Viewport cannot have zero width.");

		double yDiff1 = oldViewport[3] - oldViewport[2];
		if(yDiff1==0) throw new RuntimeException("Viewport cannot have zero height.");

		double xDiff2 = newViewport[1] - newViewport[0];
		if(xDiff2==0) throw new RuntimeException("Viewport cannot have zero width.");

		double yDiff2 = newViewport[3] - newViewport[2];
		if(yDiff2==0) throw new RuntimeException("Viewport cannot have zero height.");

		// rescale from oldViewport to [0,1]x[0,1]

		Matrix3d mat1 = new Matrix3d(
			1.0 / xDiff1, 0, -oldViewport[0] / xDiff1,
			0, 1.0 / yDiff1, -oldViewport[2] / yDiff1,
			0, 0, 1
		);

		// rescale from [0,1]x[0,1] to newViewport

		Matrix3d mat2 = new Matrix3d(
			xDiff2, 0, newViewport[0],
			0, yDiff2, newViewport[2],
			0, 0, 1
		);

		// apply the composite transform

		Matrix3d matProd = new Matrix3d();
		matProd.mul(mat2, mat1);

		return matProd;
	}
}
