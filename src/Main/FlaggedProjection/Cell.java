package FlaggedProjection;

import Geometry.Lines.ILine3d;
import Geometry.Lines.Line3d;
import Mesh.IMesh;
import Mesh.IMeshPolygon;
import Mesh.Mesh;
import Mesh.MeshPolygon;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;


/**
 * A triangular cell that makes up a larger projection screen.
 */
public class Cell extends Mesh implements ICell {

	/**
	 * The plane on which the cell lies.
	 */
	protected IPlane m_plane;

	/**
	 * Copy constructor.
	 * @param cell The cell object to copy from.
	 */
	public Cell(ICell cell) {
		this(cell.getPlane());
	}

	/**
	 * Gets the plane.
	 * @return The plane.
	 */
	public IPlane getPlane() {
		return m_plane==null ? null : new Plane(m_plane);
	}

	/**
	 * Constructs the cell from a plane and a 3D model.
	 * @param plane The plane that the cell lies in.
	 */
	public Cell(IPlane plane) {
		super(computeTriangularMesh(plane));
		m_plane = plane;
	}

	/**
	 * Checks if the object is equal to the cell.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the cell.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!IMesh.class.isAssignableFrom(obj.getClass())) return false;
		return equals((ICell)obj);
	}

	/**
	 * Checks for equality between two cells.
	 * @param cell The cell with which to compare.
	 * @return Whether the cells are equal.
	 */
	private boolean equals(ICell cell) {
		if(!m_plane.equals(cell.getPlane())) return false;
		if(!super.equals(cell)) return false;
		return true;
	}

	/**
	 * Computes the associated mesh without grid lines.
	 * @param plane The plane that defines the cell.
	 * @return The mesh containing edges and faces.
	 */
	private static IMesh computeTriangularMesh(IPlane plane) {

		Point3d p1 = plane.getOrigin();

		Point3d p2 = new Point3d(p1);
		p2.add(plane.getBasis1());

		Point3d p3 = new Point3d(p1);
		p3.add(plane.getBasis2());

		// get edges
		ILine3d[] edges = { new Line3d(p1, p2), new Line3d(p2, p3), new Line3d(p3, p1) };

		// get faces
		Vector3d normal = null;
		try{
			normal = plane.getNormal();
		}
		catch(ArithmeticException e) {
		}
		IMeshPolygon[] faces = { new MeshPolygon(new Point3d[] {p1,p2,p3}, normal, false, null) };

		return new Mesh(edges, faces);
	}
}
