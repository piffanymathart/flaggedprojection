package FlaggedProjection;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * A 2D plane embedded in 3D space represented by an origin and two basis vectors.
 */
public class Plane implements IPlane {

	/**
	 * The origin position.
	 */
	private Point3d m_origin;

	/**
	 * The first basis vector.
	 */
	private Vector3d m_basis1;

	/**
	 * The second basis vector.
	 */
	private Vector3d m_basis2;

	/**
	 * Constructs a 2D plane.
	 * @param origin The position of the origin.
	 * @param basis1 The first basis vector.
	 * @param basis2 The second basis vector.
	 */
	public Plane(Point3d origin, Vector3d basis1, Vector3d basis2) {
		m_origin = origin;
		m_basis1 = basis1;
		m_basis2 = basis2;
	}

	/**
	 * Constructs a 2D plane from three points.
	 * @param p1 The first point (the origin).
	 * @param p2 The second point (where the first basis vector extends)
	 * @param p3 The third point (where the second basis vector extends)
	 */
	public Plane(Point3d p1, Point3d p2, Point3d p3) {

		if(p1 == null || p2 == null || p3 == null) throw new RuntimeException("Inputs cannot be null");

		m_origin = p1;

		m_basis1 = new Vector3d(p2);
		m_basis1.sub(p1);

		m_basis2 = new Vector3d(p3);
		m_basis2.sub(p1);
	}

	/**
	 * Copy constructor.
	 * @param plane The plane to copy from.
	 */
	public Plane(IPlane plane) {
		m_origin = plane.getOrigin();
		m_basis1 = plane.getBasis1();
		m_basis2 = plane.getBasis2();
	}

	/**
	 * Gets the origin.
	 * @return The origin.
	 */
	public Point3d getOrigin() {
		return m_origin==null ? null : new Point3d(m_origin);
	}

	/**
	 * Gets the first basis vector.
	 * @return The first basis vector.
	 */
	public Vector3d getBasis1() {
		return m_basis1==null ? null : new Vector3d(m_basis1);
	}

	/**
	 * Gets the second basis vector.
	 * @return The second basis vector.
	 */
	public Vector3d getBasis2() {
		return m_basis2==null ? null : new Vector3d(m_basis2);
	}

	/**
	 * Computes the normal vector.
	 * @return The normal vector.
	 */
	public Vector3d getNormal() {
		Vector3d normal = new Vector3d();
		normal.cross(m_basis1, m_basis2);
		if(normal.length()==0) throw new ArithmeticException("Normal vector of the plane has length zero.");
		normal.normalize();
		return normal;
	}

	/**
	 * Checks if the object is equal to the plane.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the plane.
	 */
	@Override
	public boolean equals(Object obj) {

		if (obj == null) return false;
		if (!IPlane.class.isAssignableFrom(obj.getClass())) return false;
		final IPlane plane = (IPlane) obj;

		// compare two planes
		return m_origin.equals(plane.getOrigin())
			&& m_basis1.equals(plane.getBasis1())
			&& m_basis2.equals(plane.getBasis2());
	}
}
