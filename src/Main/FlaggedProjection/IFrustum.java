package FlaggedProjection;

/**
 * A frustum that defines a perspective projection, containing the field of view and aspect ratio.
 */
public interface IFrustum {

	/**
	 * Gets the vertical field of view.
	 * @return The vertical field of view in radians.
	 */
	double getFovY();

	/**
	 * Gets the aspect ratio.
	 * @return The aspect ratio given by width/height.
	 */
	double getAspect();

	/**
	 * Gets the z-value of the near plane.
	 * @return The z-value of the near plane.
	 */
	double getNear();

	/**
	 * Sets the vertical field of view. The value is bound by the minimum and maximum.
	 * @param fovY The vertical field of view in radians.
	 */
	void setFovY(double fovY);

	/**
	 * Sets the aspect ratio.
	 * @param aspect The aspect ratio given by width/height.
	 */
	void setAspect(double aspect);

	/**
	 * Validates the frustum parameters for perspective projection.
	 * @return Whether the parameters are valid for perspective projection.
	 */
	boolean isValid();
}
