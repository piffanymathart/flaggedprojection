package FlaggedProjection;

import javax.vecmath.Point2d;

/**
 * A helper class for clipping lines to rectangular regions.
 */
interface IClippingManager {

	/**
	 * Clips a set of points from a polygon to a clipping polygon.
	 * @param points The points from a polygon to clip.
	 * @param clipper The clipping polygon.
	 * @return The points from the clipped polygon.
	 */
	Point2d[] clipPointsToPolygon(Point2d[] points, Point2d[] clipper);
}
