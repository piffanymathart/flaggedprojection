package FlaggedProjection;

import javax.vecmath.Point2d;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A helper class for clipping lines to rectangular regions.
 */
class ClippingManager implements IClippingManager {

	/**
	 * Clips a set of points from a polygon to a clipping polygon.
	 * @param points The points from a polygon to clip.
	 * @param clipper The clipping polygon.
	 * @return The points from the clipped polygon.
	 */
	public Point2d[] clipPointsToPolygon(Point2d[] points, Point2d[] clipper) {

		List<Point2d> clipped = Arrays.asList(truncateFarPoints(points));

		// Sutherland-Hodgman algorithm
		int len = clipper.length;
		for (int i = 0; i < len; i++) {

			int len2 = clipped.size();
			List<Point2d> input = clipped;
			clipped = new ArrayList<>(len2);

			Point2d A = clipper[(i + len - 1) % len];
			Point2d B = clipper[i];

			for (int j = 0; j < len2; j++) {

				Point2d P = input.get((j + len2 - 1) % len2);
				Point2d Q = input.get(j);

				if (isInside(A, B, Q)) {
					if (!isInside(A, B, P))
						clipped.add(intersection(A, B, P, Q));
					clipped.add(Q);
				} else if (isInside(A, B, P))
					clipped.add(intersection(A, B, P, Q));
			}
		}
		return clipped.toArray(new Point2d[clipped.size()]);
	}

	/**
	 * Truncate faraway points to avoid numerical errors near floating max value.
	 * @param points Points to truncate.
	 * @return Truncated points.
	 */
	private Point2d[] truncateFarPoints(Point2d[] points) {
		int n = points.length;
		Point2d[] normPts = new Point2d[n];
		for(int i=0; i<n; i++) {
			Point2d p = points[i];
			double maxAbs = Math.max(Math.abs(p.x), Math.abs(p.y));
			if(maxAbs < 1e9) normPts[i] = p;
			else {
				double scale = 1e9/maxAbs;
				Point2d q = new Point2d(p.x*scale, p.y*scale);
				normPts[i] = q;
			}
		}
		return normPts;
	}

	/**
	 * Helper method for the clipToPolygon method.
	 */
	private boolean isInside(Point2d a, Point2d b, Point2d c) {
		return (a.x - c.x) * (b.y - c.y) > (a.y - c.y) * (b.x - c.x);
	}

	/**
	 * Helper method for the clipToPolygon method.
	 */
	private Point2d intersection(Point2d a, Point2d b, Point2d p, Point2d q) {
		double A1 = b.y - a.y;
		double B1 = a.x - b.x;
		double C1 = A1 * a.x + B1 * a.y;

		double A2 = q.y - p.y;
		double B2 = p.x - q.x;
		double C2 = A2 * p.x + B2 * p.y;

		double det = A1 * B2 - A2 * B1;
		double x = (B2 * C1 - B1 * C2) / det;
		double y = (A1 * C2 - A2 * C1) / det;

		return new Point2d(x, y);
	}
}
