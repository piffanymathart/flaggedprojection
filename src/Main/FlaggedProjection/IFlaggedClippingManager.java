package FlaggedProjection;

import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.IPolygon4d;

import javax.vecmath.Matrix4d;
import java.awt.*;
import java.util.List;
import java.util.Map;

interface IFlaggedClippingManager {

	/**
	 * Clip a set of polygons to a cell.
	 * @param flaggedPolygons The flagged polygons to clip.
	 * @param plane The plane that defines the  cell.
	 * @return A set of clipped polygons.
	 */
	Map.Entry<IPolygon3d[], Color[]> clipPolygonsToCell(List<IPolygon4d> flaggedPolygons, List<Color> polygonColours, IPlane plane);

	/**
	 * Clip a polygon to a cell.
	 * @param flaggedPolygon The flagged polygon to clip.
	 * @param transforms The matrix transform (and its inverse) that transform to cell coordinates (and back).
	 * @return A clipped polygon.
	 */
	IPolygon3d clipPolygonToCell(IPolygon4d flaggedPolygon, Matrix4d[] transforms);

	/**
	 * Sets the flagged unit clipper.
	 * @param flaggedUnitClipper The flagged unit clipper to set as.
	 */
	void setFlaggedUnitClipper(IFlaggedUnitClipper flaggedUnitClipper);
}
