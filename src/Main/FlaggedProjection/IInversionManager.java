package FlaggedProjection;

import Geometry.Lines.ILine2d;

import java.util.List;

/**
 * A helper class to manage the inversion of lines and polygons needed to
 * draw elements that are partially outside the projection plane's view.
 */
interface IInversionManager {

	/**
	 * Inverts a set of lines with their suggested projection flags.
	 * @param lines The lines.
	 * @param flags The corresponding suggested projection flags.
	 * @return The inverted lines.
	 */
	List<ILine2d> invertLines(List<ILine2d> lines, List<int[]> flags);

	/**
	 * Inverts a line with the suggested projection flags of its endpoints.
	 * @param line The line.
	 * @param flags The suggested projection flags of its endpoints.
	 * @return The inverted line.
	 */
	ILine2d invertLine(ILine2d line, int[] flags);
}
