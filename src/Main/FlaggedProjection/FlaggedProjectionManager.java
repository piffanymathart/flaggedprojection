package FlaggedProjection;

import Geometry.Lines.ILine3d;
import Geometry.Lines.Line3d;
import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.IPolygon4d;
import Geometry.Polygons.Polygon3d;
import Geometry.Polygons.Polygon4d;
import Mesh.*;

import javax.vecmath.Point3d;
import javax.vecmath.Point4d;
import javax.vecmath.Vector3d;
import java.awt.*;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A helper class for managing projections of flagged points, lines, and faces. A 3D flagged
 * point contains an extra 4th coordinate that we call the perspective flag. This flag indicates
 * whether a point is in front of (flag = 1) or behind the eye (flag = -1). It is a result of performing
 * a perspective projection.
 *
 * Typically, one would not render such a point because it does not lie on the projection plane. However,
 * if a line connects two points whose flag values are 1 and -1, then this line will partially lie on the
 * projection plane. Therefore we need to retain the point with flag = -1 in order to perform the line
 * clipping. Since this point does not have a physical position in 3-space, the 4th coordinate is required
 * to properly represent its position and state.
 */
public class FlaggedProjectionManager implements IFlaggedProjectionManager {

	/**
	 * Manages polygon clipping in flagged space.
	 */
	private IFlaggedClippingManager m_flaggedClippingManager = new FlaggedClippingManager();

	/**
	 * The normals of the frustum plane representing the cell's cone of vision.
	 */
	private Vector3d[] m_frustumPlaneNormals;

	/**
	 * The dot products of the frustum plane points and the plane normals representing the cell's cone of vision.
	 */
	private double[] m_frustumPlanePtDotNormals;

	/**
	 * Projects and clips a 3D model from the origin to a plane.
	 * @param obj The 3D model to project.
	 * @param cell The cell on which to project.
	 * @return The projected and clipped model on the cell.
	 */
	public IMesh projectAndClipToCell(IMesh obj, ICell cell) {

		if(obj==null || cell==null) return null;

		// check for degenerate cells
		try {
			cell.getPlane().getNormal();
		}
		catch(ArithmeticException e) {
			return new Mesh();
		}

		// project and clip the edges (need to convert to polygons and back)
		IPlane[] frustumPlanes = getCellFrustum(cell);

		int planeCount = frustumPlanes.length;
		m_frustumPlaneNormals = new Vector3d[planeCount];
		m_frustumPlanePtDotNormals = new double[planeCount];

		for(int i = 0; i< planeCount; i++) {
			IPlane plane = frustumPlanes[i];
			try {
				m_frustumPlaneNormals[i] = plane.getNormal();
			}
			catch(ArithmeticException e) {
				// normal vector has length zero, which means cell is parallel to line of sight, so there's no projection
				return new Mesh();
			}
			m_frustumPlanePtDotNormals[i] = new Vector3d(plane.getOrigin()).dot(m_frustumPlaneNormals[i]);
		}

		ILine3d[] insideEdges = removeLinesOutsideCellFrustum(obj.getEdges());
		IMeshLine[] clippedEdges = projectAndClipLines(insideEdges, cell);

		Map.Entry<IPolygon3d[], Color[]> insideFaces = removePolygonsOutsideCellFrustum(
			obj.getFaces(),
			obj.getFaceColours()
		);
		IMeshPolygon[] clippedFaces = projectAndClipPolygons(
			insideFaces.getKey(),
			insideFaces.getValue(),
			cell
		);

		// create projected object
		IMesh projected = new Mesh(clippedEdges, clippedFaces);

		return projected;
	}

	/**
	 * Projects and clips a set of coloured polygons onto a cell.
	 * @param polygons The polygons.
	 * @param polygonColours The polygon colours.
	 * @param cell The cell.
	 * @return A pair of polygons with colours.
	 */
	public IMeshPolygon[]  projectAndClipPolygons(IPolygon3d[] polygons, Color[] polygonColours, ICell cell) {

		if(polygons.length==0) return new IMeshPolygon[0];

		// get cell data
		IPlane plane = cell.getPlane();
		Point3d planePt = plane.getOrigin();
		Vector3d normal = plane.getNormal();

		double planePtDotNormal = new Vector3d(planePt).dot(normal);

		// apply projection
		List<IPolygon4d> projections = new ArrayList<>();
		List<Color> projectionColours = new ArrayList<>();

		for (int i = 0; i < polygons.length; i++) {
			IPolygon4d projection = projectPolygonToPlaneFast(polygons[i], normal, planePtDotNormal);
			if (projection.size() > 1) {
				projections.add(projection);
				if(polygonColours != null) projectionColours.add(polygonColours[i]);
			}
		}

		// perform clipping
		Map.Entry<IPolygon3d[], Color[]> clippedPolygons = m_flaggedClippingManager.clipPolygonsToCell(
			projections,
			(polygonColours==null) ? null : projectionColours,
			plane
		);

		return toMeshPolygon(clippedPolygons.getKey(), clippedPolygons.getValue());
	}

	private IMeshPolygon[] toMeshPolygon(IPolygon3d[] polygons, Color[] colours) {
		int n = polygons.length;
		IMeshPolygon[] meshPolygons = new IMeshPolygon[n];
		for(int i=0; i<n; i++) {
			meshPolygons[i] = new MeshPolygon(polygons[i].getVertices(), null, false, (colours==null ? null : colours[i]));
		}
		return meshPolygons;
	}

	/**
	 * Computes the projection of a 3D point from the origin to a plane using a faster merhod
	 * involving precomputed values.
	 * @param pt The 3D point to project.
	 * @param planeNormal The 3D normal vector of the plane.
	 * @param planePtDotNormal The dot product of the plane point and the outward plane normal.
	 * @return A flagged 3D point representing the projected point that lies on the target plane.
	 */
	public Point4d projectPointToPlaneFast(Point3d pt, Vector3d planeNormal, double planePtDotNormal) {

		// if the eye-point ray intersects the plane, then return the intersection point
		double d = planePtDotNormal / (pt.x*planeNormal.x + pt.y*planeNormal.y + pt.z*planeNormal.z);

		Point4d newP = new Point4d(
			d * pt.x,
			d * pt.y,
			d * pt.z,
			(d >= 0) ? 1 : -1
		);
		return newP;
	}

	/**
	 * Sets the flagged clipping manager.
	 * @param flaggedClippingManager The flagged clipping manager to set to.
	 */
	public void setFlaggedClippingManager(IFlaggedClippingManager flaggedClippingManager) {
		m_flaggedClippingManager = flaggedClippingManager;
	}

	/**
	 * Computes the projection of a 3D polygon from the origin to a plane using a faster merhod
	 * involving precomputed values.
	 * @param polygon The 3D polygon to project.
	 * @param planeNormal The 3D normal vector of the plane.
	 * @param planePtDotNormal The dot product of the plane point and the outward plane normal.
	 * @return A flagged 3D polygon representing the projected polygon that lies on the target plane.
	 */
	private IPolygon4d projectPolygonToPlaneFast(IPolygon3d polygon, Vector3d planeNormal, double planePtDotNormal) {
		int n = polygon.size();
		Point4d[] verts = new Point4d[n];
		for(int i=0; i<n; i++) {
			verts[i] = projectPointToPlaneFast(polygon.get(i), planeNormal, planePtDotNormal);
		}
		return new Polygon4d(verts);
	}

	/**
	 * Removes lines outside the cell frustum.
	 * @param lines The lines.
	 * @return The lines inside the cell frustum.
	 */
	private ILine3d[] removeLinesOutsideCellFrustum(ILine3d[] lines) {

		List<ILine3d> insideLines = new ArrayList<>();
		for(ILine3d line : lines) {
			if(isInsideFrustum(line)) insideLines.add(line);
		}
		return insideLines.toArray(new ILine3d[insideLines.size()]);
	}

	/**
	 * Removes polygons outside the cell frustum.
	 * @param polygons The polygons.
	 * @return The polygons inside the cell frustum.
	 */
	private Map.Entry<IPolygon3d[], Color[]> removePolygonsOutsideCellFrustum(IPolygon3d[] polygons, Color[] polygonColours) {
		List<IPolygon3d> insidePolygons = new ArrayList<>();
		List<Color> insidePolygonColours = new ArrayList<>();
		for(int i=0; i<polygons.length; i++) {
			if(isInsideFrustum(polygons[i])) {
				insidePolygons.add(polygons[i]);
				if(polygonColours!=null) insidePolygonColours.add(polygonColours[i]);
			}
		}
		Map.Entry<IPolygon3d[], Color[]> result = new AbstractMap.SimpleEntry<>(
			insidePolygons.toArray(new IPolygon3d[insidePolygons.size()]),
			(polygonColours==null) ? null : insidePolygonColours.toArray(new Color[insidePolygonColours.size()])
		);

		return result;
	}

	/**
	 * Computes the cell frustum from the eye to the cell.
	 * @param cell The cell.
	 * @return The cell frustum.
	 */
	private IPlane[] getCellFrustum(ICell cell) {
		IPlane plane = cell.getPlane();

		Vector3d v1 = new Vector3d(plane.getOrigin());
		Vector3d b1 = plane.getBasis1();
		Vector3d b2 = plane.getBasis2();

		Vector3d v2 = new Vector3d(); v2.add(v1,b1);
		Vector3d v3 = new Vector3d(); v3.add(v2,b2);
		Vector3d v4 = new Vector3d(); v4.add(v1,b2);

		Point3d o = new Point3d(0,0,0);
		// I don't understand why using the scalar triple product works
		if(scalarTripleProduct(v1,b1,b2) < 0) {
			return new Plane[] {
				new Plane(o, v1, v2),
				new Plane(o, v2, v3),
				new Plane(o, v3, v4),
				new Plane(o, v4, v1)
			};
		}
		else {
			return new Plane[]  {
				new Plane(o, v2, v1),
				new Plane(o, v3, v2),
				new Plane(o, v4, v3),
				new Plane(o, v1, v4)
			};
		}
	}

	/**
	 * Computes the scalar triple product: (v1 x v2).origin.
	 * @param origin The origin.
	 * @param v1 The first vector.
	 * @param v2 The second vector.
	 * @return The scalar triple product: (v1 x v2).origin.
	 */
	private double scalarTripleProduct(Vector3d origin, Vector3d v1, Vector3d v2) {
		return origin.x * (v1.y*v2.z - v1.z*v2.y)
			- origin.y * (v1.x*v2.z - v1.z*v2.x)
			+ origin.z * (v1.x*v2.y - v1.y*v2.x);
	}

	/**
	 * Checks whether a polygon is inside the frustum.
	 * @param polygon The polygon.
	 * @return Whether a polygon is inside the frustum.
	 */
	private boolean isInsideFrustum(IPolygon3d polygon) {

		for(int i = 0; i< m_frustumPlaneNormals.length; i++) {
			if(isOutsidePlane(polygon, m_frustumPlaneNormals[i], m_frustumPlanePtDotNormals[i])) return false;
		}
		return true;
	}

	/**
	 * Checks whether a line is inside the frustum.
	 * @param line The line.
	 * @return Whether a line is inside the frustum.
	 */
	private boolean isInsideFrustum(ILine3d line) {

		for(int i = 0; i< m_frustumPlaneNormals.length; i++) {
			if(isOutsidePlane(line, m_frustumPlaneNormals[i], m_frustumPlanePtDotNormals[i])) return false;
		}
		return true;
	}

	/**
	 * Checks whether a polygon is on the inside of a plane given some recomputed values.
	 * @param polygon The polygon.
	 * @param planeNormal The outward plane normal.
	 * @param planePtDotNormal The dot product of the plane point and the outward plane normal.
	 * @return Whether a polygon is on the inside of the plane.
	 */
	private boolean isOutsidePlane(IPolygon3d polygon, Vector3d planeNormal, double planePtDotNormal) {

		for(Point3d v : polygon.getVertices()) {
			if(isInsidePlane(v, planeNormal, planePtDotNormal)) return false;
		}
		return true;
	}

	/**
	 * Checks whether a line is on the outside of a plane given some recomputed values.
	 * @param line The line.
	 * @param planeNormal The outward plane normal.
	 * @param planePtDotNormal The dot product of the plane point and the outward plane normal.
	 * @return Whether a line is on the outside of the plane.
	 */
	private boolean isOutsidePlane(ILine3d line, Vector3d planeNormal, double planePtDotNormal) {
		return isOutsidePlane(line.getP1(), planeNormal, planePtDotNormal)
			&& isOutsidePlane(line.getP2(), planeNormal, planePtDotNormal);
	}

	/**
	 * Checks whether a point is on the outside of a plane given some recomputed values.
	 * @param pt The point.
	 * @param planeNormal The outward plane normal.
	 * @param planePtDotNormal The dot product of the plane point and the outward plane normal.
	 * @return Whether a point is on the outside of the plane.
	 */
	private boolean isOutsidePlane(Point3d pt, Vector3d planeNormal, double planePtDotNormal) {
		// (pt - planePt).dot(planeNormal) >= 0
		return (pt.x*planeNormal.x + pt.y*planeNormal.y + pt.z*planeNormal.z) >= planePtDotNormal;
	}

	/**
	 * Checks whether a point is on the inside of a plane given some recomputed values.
	 * @param pt The point.
	 * @param planeNormal The outward plane normal.
	 * @param planePtDotNormal The dot product of the plane point and the outward plane normal.
	 * @return Whether a point is on the inside of the plane.
	 */
	private boolean isInsidePlane(Point3d pt, Vector3d planeNormal, double planePtDotNormal) {
		// (pt - planePt).dot(planeNormal) < 0
		return (pt.x*planeNormal.x + pt.y*planeNormal.y + pt.z*planeNormal.z) < planePtDotNormal;
	}

	/**
	 * Projects and clips a set of lines to a cell.
	 * @param lines The lines.
	 * @param cell The cell.
	 * @return The set of projected and clipped lines.
	 */
	private IMeshLine[] projectAndClipLines(ILine3d[] lines, ICell cell) {

		if(lines.length==0) return new IMeshLine[0];

		// get cell data
		IPlane plane = cell.getPlane();
		Point3d planePt = plane.getOrigin();
		Vector3d normal = plane.getNormal();

		double planePtDotNormal = new Vector3d(planePt).dot(normal);

		// apply projection
		List<IPolygon4d> projections = new ArrayList<>();
		for(ILine3d line : lines) {
			IPolygon4d projection = projectPolygonToPlaneFast(lineToPolygon(line), normal, planePtDotNormal);
			if(projection.size()>1) projections.add(projection);
		}

		// perform clipping
		Map.Entry<IPolygon3d[], Color[]> clippedPolygons = m_flaggedClippingManager.clipPolygonsToCell(projections, null, plane);

		return toMeshLine(polygonsToLines(clippedPolygons.getKey()));
	}

	private IMeshLine[] toMeshLine(ILine3d[] lines) {
		int n = lines.length;
		IMeshLine[] meshLines = new IMeshLine[n];
		for(int i=0; i<n; i++) {
			meshLines[i] = new MeshLine(lines[i]);
		}
		return meshLines;
	}

	/**
	 * Converts a line to a polygon.
	 * @param line The line.
	 * @return The converted polygon.
	 */
	private IPolygon3d lineToPolygon(ILine3d line) {
		return new Polygon3d(line.getP1(), line.getP2());
	}

	/**
	 * Converts polygons to lines, assuming the polygons only have two vertices each.
	 * @param polygons The polygons.
	 * @return The converted lines..
	 */
	private ILine3d[] polygonsToLines(IPolygon3d[] polygons) {
		int n = polygons.length;
		ILine3d[] lines = new ILine3d[n];
		for(int i=0; i<n; i++) {
			lines[i] = new Line3d(polygons[i].get(0), polygons[i].get(1));
		}
		return lines;
	}
}
