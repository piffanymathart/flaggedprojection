package FlaggedProjection;

/**
 * A frustum that defines a perspective projection from the field of view and aspect ratio
 */
public class Frustum implements IFrustum {

	/**
	 * The vertical field of view in radians.
	 */
	private double m_fovY;

	/**
	 * The aspect ratio given by width/height.
	 */
	private double m_aspect;

	/**
	 * The z-value of the near plane.
	 */
	private final double m_near = -1e-5;

	/**
	 * The minimum vertical field of view in radians.
	 */
	public static final double MIN_FOV_Y = 0.1;

	/**
	 * The maximum vertical field of view in radians.
	 */
	public static final double MAX_FOV_Y = Math.PI - 0.1;

	/**
	 * Constructs a frustum from field of view and aspect ratio.
	 * @param fovY The vertical field of view in radians.
	 * @param aspect The aspect ratio given by width/height.
	 */
	public Frustum(double fovY, double aspect) {
		m_fovY = fovY;
		m_aspect = aspect;
	}

	/**
	 * Copy constructor.
	 * @param frustum The frustum to copy from.
	 */
	public Frustum(IFrustum frustum) {
		m_fovY = frustum.getFovY();
		m_aspect = frustum.getAspect();
	}

	/**
	 * Gets the vertical field of view.
	 * @return The vertical field of view in radians.
	 */
	public double getFovY() {
		return m_fovY;
	}

	/**
	 * Gets the aspect ratio.
	 * @return The aspect ratio given by width/height.
	 */
	public double getAspect() {
		return m_aspect;
	}

	/**
	 * Gets the z-value of the near plane.
	 * @return The z-value of the near plane.
	 */
	public double getNear() { return m_near; }

	/**
	 * Sets the aspect ratio.
	 * @param aspect The aspect ratio given by width/height.
	 */
	public void setAspect(double aspect) {
		m_aspect = aspect;
	}

	/**
	 * Sets the vertical field of view. The value is bound by the minimum and maximum.
	 * @param fovY The vertical field of view in radians.
	 */
	public void setFovY(double fovY) {
		if(fovY < MIN_FOV_Y) m_fovY = MIN_FOV_Y;
		else if(fovY > MAX_FOV_Y) m_fovY = MAX_FOV_Y;
		else m_fovY = fovY;
	}

	/**
	 * Validates the frustum parameters for perspective projection.
	 * @return Whether the parameters are valid for perspective projection.
	 */
	public boolean isValid() {
		if(m_fovY <= MIN_FOV_Y || m_fovY >= MAX_FOV_Y) return false;
		if(m_aspect<=0) return false;
		return true;
	}
}
