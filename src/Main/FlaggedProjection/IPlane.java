package FlaggedProjection;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * A 2D plane embedded in 3D space represented by an origin and two basis vectors.
 */
public interface IPlane {

	/**
	 * Gets the origin.
	 * @return The origin.
	 */
	Point3d getOrigin();

	/**
	 * Gets the first basis vector.
	 * @return The first basis vector.
	 */
	Vector3d getBasis1();

	/**
	 * Gets the second basis vector.
	 * @return The second basis vector.
	 */
	Vector3d getBasis2();

	/**
	 * Computes the normal vector.
	 * @return The normal vector.
	 */
	Vector3d getNormal();

	/**
	 * Checks if the object is equal to the plane.
	 * @param obj The object with which to compare.
	 * @return Whether the object is equal to the plane.
	 */
	@Override
	boolean equals(Object obj);
}
